# https://adventofcode.com/2020/day/25

function transform(loop_size, subject = 7)
    res = 1
    for _ in 1:loop_size
        res *= subject
        res %= 20201227
    end
    res
end

function loop_size(key, subject = 7)
    res = 1
    i = 0
    while res != key
        res *= subject
        res %= 20201227
        i += 1
    end
    i
end

door_public_key = 8252394
card_public_key = 6269621
transform(loop_size(door_public_key), card_public_key)
#  == transform(loop_size(card_public_key), door_public_key)
