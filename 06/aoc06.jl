# https://adventofcode.com/2020/day/6

function parse_input(file :: String) :: Array{Array{String,1},1}
    groups = Array{String, 1}[]
    open(file) do f
        answers = String[]
        for line in eachline(f)
            if line == ""
                push!(groups, answers)
                answers = String[]
            else
                push!(answers, line)
            end
        end
        push!(groups, answers)
    end
    groups
end

function destructurize(x)
    map(x -> split(x, ""), x)
end

function solve(groups_answers, f_agreg)
    g2 = destructurize(groups_answers)
    reduce(f_agreg, g2) |> length
end

function solve1(input)
    map(x -> solve(x, union), input) |> sum
end

function solve2(input)
    map(x -> solve(x, intersect), input) |> sum
end

input = parse_input("input.txt")
println("Part 1 : $(solve1(input))")
println("Part 2 : $(solve2(input))")
