# https://adventofcode.com/2020/day/2

struct Password
    range :: Tuple{Int64,Int64}
    char :: Char
    pwd :: String
end

function parse_input(file :: String) :: Array{Password, 1}
    res = Password[]
    open(file) do f
        for line in eachline(f)
            infos = split(line, " ")
            range = map(x -> parse(Int, x), split(infos[1], "-"))
            push!(res, Password((range[1], range[2]), infos[2][1], infos[3]))
        end
    end
    res
end

function is_correct_1(p :: Password) :: Bool
    n = 0
    for i in 1:length(p.pwd)
        if p.pwd[i] == p.char
            n += 1
        end
    end
    p.range[1] <= n <= p.range[2]
end

function is_correct_2(p :: Password) :: Bool
    xor(
        p.pwd[p.range[1]] == p.char,
        p.pwd[p.range[2]] == p.char
    )
end

function solve(file :: String, part :: Int) :: Int
    pwds = parse_input(file)
    correct = 0
    is_correct = part == 1 ? is_correct_1 : is_correct_2
    for i in 1:length(pwds)
        if is_correct(pwds[i])
            correct += 1
        end
    end
    correct
end

println("Part 1 : $(solve("input.txt", 1))")
println("Part 2 : $(solve("input.txt", 2))")
