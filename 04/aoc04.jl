# https://adventofcode.com/2020/day/4

function parse_input(file :: String) :: Array{Dict{String, String}, 1}
    res = String[]
    open(file) do f
        p_string = ""
        for line in eachline(f)
            if line == ""
                push!(res, p_string)
                p_string = ""
            else
                p_string = strip(string(p_string, " ", line))
            end
        end
        push!(res, p_string)
    end
    splitted = map(x -> split(x, " "), res)
    map(splitted) do fields
        res = Dict()
        for field in fields
            res[SubString(field, 1, 3)] = SubString(field, 5)
        end
        res
    end
end

function all_fields(ppt) :: Bool
    fields = sort(collect(keys(ppt)))
    expected1 = ["byr", "ecl", "eyr", "hcl", "hgt", "iyr", "pid"]
    expected2 = ["byr", "cid", "ecl", "eyr", "hcl", "hgt", "iyr", "pid"]
    (fields == expected1) || (fields == expected2)
end

function valid(ppt) :: Bool

    if (!all_fields(ppt)) ; return false ; end

    byr = try
            byr = parse(Int, ppt["byr"])
          catch e
            return false
          end
    if !(1920 <= byr <= 2002) ; return false ; end

    iyr = try 
            parse(Int, ppt["iyr"])
          catch e
            return false
          end
    if !(2010 <= iyr <= 2020) ; return false ; end

    eyr = try
            parse(Int, ppt["eyr"])
          catch e
            return false
          end
    if !(2020 <= eyr <= 2030) ; return false ; end

    hgt = match(r"^(\d{1,3})(cm|in)$", ppt["hgt"])
    if hgt == nothing ; return false ; end
    if hgt[2] == "cm" && !(150 <= parse(Int, hgt[1]) <= 193) ; return false ; end
    if hgt[2] == "in" && !(59 <= parse(Int, hgt[1]) <= 76) ; return false ; end

    if !(occursin(r"^#[0-9a-f]{6}$", ppt["hcl"])) ; return false ; end

    if !(ppt["ecl"] in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"])
        return false
    end

    if !(occursin(r"^\d{9}$", ppt["pid"])) ; return false ; end

    true

end

function solve1(file) :: Int
    input = parse_input(file)
    sum(map(all_fields, input))
end

function solve2(file) :: Int
    input = parse_input(file)
    sum(map(valid, input))
end

println("Part 1 : $(solve1("input.txt"))")
println("Part 2 : $(solve2("input.txt"))")
