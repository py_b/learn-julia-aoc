# https://adventofcode.com/2020/day/8

struct Inst
    operation :: String # "nop" | "acc" | "jmp"
    argument :: Int
end

mutable struct Machine
    insts :: Array{Inst, 1}
    current :: Int
    visited :: Array{Int, 1}
    acc :: Int
end

function inst_from_str(val :: String) :: Inst
    res = split(val, " ")
    Inst(res[1], parse(Int, res[2]))
end

function parse_input(file :: String) :: Array{Inst, 1}
    map(inst_from_str, readlines(file))
end

function switch_jmp_nop(insts, k) :: Array{Inst,1}
    # reconstruct list of Inst as it is immutable
    operations = map(x -> x.operation, insts)
    arguments = map(x -> x.argument, insts)
    if operations[k] == "nop"
        operations[k] = "jmp"
    elseif operations[k] == "jmp"
        operations[k] = "nop"
    end
    map(Inst, operations, arguments)
end

function run(insts :: Array{Inst, 1}) :: Tuple{Int64, Bool}
    size = length(insts)
    res = Machine(insts, 1, Int[], 0)
    while !(res.current in res.visited || res.current == size)
        next!(res, insts[res.current])
    end
    terminates = res.current == size
    if terminates ; next!(res, insts[size]) ; end
    (res.acc, terminates)
end

function next!(m :: Machine, i :: Inst) :: Machine
    push!(m.visited, m.current)
    if i.operation == "nop"
        m.current += 1
    elseif i.operation == "acc"
        m.acc += i.argument
        m.current += 1
    elseif i.operation == "jmp"
        m.current += i.argument
    end
    m
end

function part1(insts :: Array{Inst, 1}) :: Int
    run(insts)[1]
end

function part2(insts :: Array{Inst, 1}) :: Int
    for i in 1:length(insts)
        if insts[i].operation == "acc"
            continue
        end
        res = run(switch_jmp_nop(insts, i))
        if res[2]
            return res[1]
        end
    end
    println("no machine terminated")
end

input = parse_input("input.txt")

println("Part 1 : $(part1(input))")
println("Part 2 : $(part2(input))")
