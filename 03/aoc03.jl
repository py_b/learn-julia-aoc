# https://adventofcode.com/2020/day/3

function seq_coords(dcol :: Int, drow :: Int, ncol :: Int, nrow :: Int) :: Array{Tuple{Int, Int}, 1}
    res = [(1, 1)]
    row = 1
    while row < nrow
        row = res[end][1] + drow
        col = (res[end][2] + dcol) % ncol
        col = col == 0 ? ncol : col # correctif quand modulo vaut 0
        push!(res, (row, col))
    end
    res
end

function solve1(file :: String, dcol :: Int, drow :: Int) :: Int

    treemap = readlines(file)
    nrow = length(treemap)
    ncol = length(treemap[1])

    path =
        map(
            coords -> treemap[coords[1]][coords[2]],
            seq_coords(dcol, drow, ncol, nrow)
        )

    length(filter(x -> x == '#', path))

end

function solve2(file :: String, slopes :: Array{Tuple{Int, Int}, 1}) :: Int
    res = 1
    for slope in slopes
        res = res * solve1(file, slope[1], slope[2])
    end
    res
end

println("Part 1 : $(solve1("input.txt", 3, 1))")
println("Part 2 : $(solve2("input.txt", [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]))")
