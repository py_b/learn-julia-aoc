# https://adventofcode.com/2020/day/1

function parse_input(file :: String) :: Array{Int, 1}
    nbs = Int[]
    open(file) do f
        for line in eachline(f)
            push!(nbs, parse(Int, line))
        end
    end
    nbs
end

function solve1(nbs :: Array{Int, 1}) :: Int
    for i in 1:length(nbs)
        for j in (i+1):length(nbs)
            if nbs[i] + nbs[j] == 2020
                return(nbs[i] * nbs[j])
            end
        end
    end
    -1
end

function solve2(nbs :: Array{Int, 1}) :: Int
    for i in 1:length(nbs)
        for j in (i+1):length(nbs)
            for k in (j+1):length(nbs)
                if nbs[i] + nbs[j] + nbs[k] == 2020
                    return(nbs[i] * nbs[j] * nbs[k])
                end
            end
        end
    end
    -1
end

function solve(file :: String, part :: Int) :: Int
    nbs = parse_input(file)
    solve_f = part == 1 ? solve1 : solve2
    solve_f(nbs)
end

println("Part 1 : $(solve("input.txt", 1))")
println("Part 2 : $(solve("input.txt", 2))")
