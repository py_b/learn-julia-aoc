# https://adventofcode.com/2020/day/5

function FBRL_to_bin(code :: AbstractString, char_one :: Char) :: Int
    zero_one = map(x -> x == char_one ? '1' : '0', code)
    parse(Int, zero_one, base = 2)
end

function seat_id(code :: String) :: Int
    row = FBRL_to_bin(SubString(code, 1 , 7), 'B')
    col = FBRL_to_bin(SubString(code, 8, 10), 'R')
    8 * row + col
end

function solve1(input :: Array{String,1}) :: Int
    maximum(map(seat_id, input))
end

function solve2(input :: Array{String,1}) :: Int
    taken = sort(map(seat_id, input))
    seats = collect(taken[1]:taken[end])
    setdiff(seats, taken)[1]
end

input = readlines("input.txt")
println("Part 1 : $(solve1(input))")
println("Part 2 : $(solve2(input))")
